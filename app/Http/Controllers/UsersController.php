<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Repository\UserRepository;


/**
 * Class UsersController.
 *
 * @package App\Http\Controllers
 */
class UsersController extends BaseController
{
    /**
     * Get index.
     *
     * @param UserRepositorysitory $userRepository
     *
     * @return mixed
     */
    public function findUser(UserRepositorysitory $userRepository, int $number)
    {
        $users = $userRepository->findByNumber($number);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Post index.
     *
     * @param UserCreateService $userCreateService
     * @param Request $request
     *
     * @return mixed
     */
    public function getIndex(UserRepositorysitory $userRepository,)
    {
      $users = $userRepository->getAll();

      return view('admin.users.index', compact('users'));
    }

}
