# Simple Repository example

Prezentowany przyklad sklada sie z nastepujacych klas:

### Controllers
- Http/Controllers/BaseController - bazowy kontroler
- Http/Controllers/UsersController - przykladowy kontroler do obslugi zadan zwiazanych z uzytkownikami

### Models
- Models/PaymentCard - przykladowy model karty kredytowej
- Models/User - przykladowy model uzytkownikami

### Repository
- Repository/Payment/PaymentCardRepositorysitory  - repozytorium dla kart kredytowych
- Repository/BaseRepository - repozytorium bazowe
- Repository/UserRepositorysitory - repozytorium dla użytkowników

### Migrations
- database/migrations/CreateUsersTable - migracja dla tabeli użytkownikow
- database/migrations/CreateUsersRolesTable - migracja dla ról uzytkownikow
- database/migrations/PaymentCardTable - migracja tabeli kart kredytowych

### Resources
- resources/views/index.blade.php - widok z listą użytkowników

### Routes
- routes/UsersRoutes - routing dla kontrolera użytkownikow

### Tests
- Models/PaymentCardTest - testy dla Modelu Payment card
- Models/UserTest - testy dla modelu User Tests
- Repository/Payment/PaymentCardRepositoryTest - testy dla payment card repository
- Repository/UserRepositoryTest - testy dla user repository


### Schemat obsługi żądania:
- żądanie trafia do Service Provider
- routing
- wybrany kontroler
- kontroler odwołuje sie do repozytorium
- repozytorium odwoluje sie do modelu i bazy
- zwracany wynik wraca "na górę" i w kontrolerze przekazywany jest do widoku


### Zadanie
- przebudować wybraną przez siebie aplikację by działała za pomocą wzorca Repository
- dopisać testy 
