<?php

namespace Tests\Repository\Payment;

use Tests\TestCase;
use App\Models\User;
use App\Models\PaymentCard;
use Illuminate\Support\MessageBag;
use App\Repository\Payment\PaymentCardRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class PaymentCardRepositoryTest.
 */
class PaymentCardRepositorysitory extends TestCase
{
    use RefreshDatabase;

    /**
     * Test create payment card.
     */
    public function testCreatePaymentCardRepositoryTest()
    {
        $paymentCardRepo = app()->make(PaymentCardRepository::class);

        $user = factory(User::class)->create();
        $paymentCard = factory(PaymentCard::class)
            ->make(['user_id' => $user->id, 'number' => '****123459', 'token' => '12345956'])->toArray()
        ;

        $this->assertInstanceOf(PaymentCard::class, $paymentCardRepo->createPaymentCard($paymentCard));
        $this->assertDatabaseHas('payment_cards', ['user_id' => $user->id, 'number' => '****123459', 'token' => '12345956']);
    }

    /*
     * Test find payment cards by user
     */
    public function testFindByUser()
    {
        $user = factory(User::class)->create();

        $paymentCardRepo = app()->make(PaymentCardRepository::class);

        $nonExistingPaymentCard = $paymentCardRepo->findByUser($user->id);
        $this->assertInstanceOf(Collection::class, $nonExistingPaymentCard);
        $this->assertEquals(0, $nonExistingPaymentCard->count());

        $paymentCard = factory(PaymentCard::class)
            ->create(['user_id' => $user->id, 'number' => '****123459', 'token' => '12345956']);


        $existingPaymentCardSearch = $paymentCardRepo->findByUser($user->id);

        $this->assertInstanceOf(Collection::class, $existingPaymentCardSearch);
        $this->assertEquals(1, $existingPaymentCardSearch->count());

    }

    /*
     * Test find payment card by number
     */
    public function findByNumber()
    {
        $user = factory(User::class)->create();

        $paymentCardRepo = app()->make(PaymentCardRepository::class);

        $nonExistingPaymentCard = $paymentCardRepo->findByNumber($user->id);
        $this->assertInstanceOf(PaymentCard::class, $nonExistingPaymentCard);
        $this->assertEquals(0, $nonExistingPaymentCard->count());

        $paymentCard = factory(PaymentCard::class)
            ->create(['user_id' => $user->id, 'number' => '****123459', 'token' => '12345956']);


        $existingPaymentCardSearch = $paymentCardRepo->findByNumber('****123459');

        $this->assertInstanceOf(PaymentCard::class, $existingPaymentCardSearch);
        $this->assertEquals(1, $existingPaymentCardSearch->count());

    }
}
