<?php

namespace Tests\Repository;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use App\Models\Store;
use App\Models\Order;
use DriverFactoryHelper;
use App\Repository\UserRepositorysitor;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class UserRepositoryTest.
 */
class UserRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test find user.
     */
    public function testFindUser()
    {
        $userRepo = app()->make(UserRepository::class);

        $nonExistingUserSearch = $userRepo->findUser('69115666');

        $this->assertInstanceOf(Collection::class, $nonExistingUserSearch);
        $this->assertEquals(0, $nonExistingUserSearch->count());

        $user = factory(User::class)->create(['phone_number' => '69115666']);
        $existingUserSearch = $userRepo->findUser('69115666');

        $this->assertInstanceOf(Collection::class, $existingUserSearch);
        $this->assertEquals(1, $existingUserSearch->count());
        $this->assertEquals($user->id, $existingUserSearch->first()->id);
    }
 /**
     * Test find user from whole group.
     */
    public function testFindUserFromAll()
    {
        $userRepo = app()->make(UserRepository::class);

        $nonExistingUserSearch = $userRepo->findUser('69115666');

        $this->assertInstanceOf(Collection::class, $nonExistingUserSearch);
        $this->assertEquals(0, $nonExistingUserSearch->count());

        $user = factory(User::class)->create(['phone_number' => '69115666', 'active' => '0']);
        $existingUserSearch = $userRepo->findUserFromAll('69115666');

        $this->assertInstanceOf(Collection::class, $existingUserSearch);
        $this->assertEquals(1, $existingUserSearch->count());
        $this->assertEquals($user->id, $existingUserSearch->first()->id);
    }
    /**
     * Test find customer multiple fields.
     */
    public function testFindCustomerMultipleFields()
    {
        $userRepo = app()->make(UserRepository::class);

        $nonExistingUserSearch = $userRepo->findCustomerMultipleFields('69115666');

        $this->assertInstanceOf(Collection::class, $nonExistingUserSearch);
        $this->assertEquals(0, $nonExistingUserSearch->count());

        factory(User::class)->create(['phone_number' => '69115666']);
        $existingUserSearch = $userRepo->findCustomerMultipleFields('69115666');

        $this->assertInstanceOf(Collection::class, $existingUserSearch);
        $this->assertEquals(1, $existingUserSearch->count());
        $this->assertInstanceOf(LengthAwarePaginator::class, $userRepo->findCustomerMultipleFields('69115666', false));
    }

    /**
     * Test update internal notes.
     */
    public function testUpdateInternalNotes()
    {
        $userRepo = app()->make(UserRepository::class);

        $user = factory(User::class)->create(['internal_notes' => 'Test Internal Notes']);

        $this->assertInstanceOf(User::class, $userRepo->updateInternalNotes($user->id, 'Updated Test Internal Notes'));
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'internal_notes' => 'Test Internal Notes']);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'internal_notes' => 'Updated Test Internal Notes']);
    }

    /**
     * Test update internal notes.
     */
    public function testUpdateNames()
    {
        $userRepo = app()->make(UserRepository::class);

        $user = factory(User::class)->create();
        $firstname = $user->first_name;

        $this->assertInstanceOf(User::class, $userRepo->updateNames($user->id, "Firstname", "Lastname"));
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'first_name' => $firstname]);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'first_name' => 'Firstname',  'last_name' => 'Lastname']);
    }
    /**
     * Test get normal users.
     */
    public function testGetNormalUsers()
    {
        $userRepo = app()->make(UserRepository::class);

        $userWithoutAnyRole = factory(User::class)->create();
        $userWithRole = factory(User::class)->create();
        $userWithRole->roles()->sync([factory(Role::class)->create()->id]);

        $normalUsersList = $userRepo->getNormalUsers();

        $this->assertInstanceOf(Collection::class, $normalUsersList);

        $emailsList = $normalUsersList->pluck('email')->toArray();

        $this->assertTrue(in_array($userWithoutAnyRole->email, $emailsList));
        $this->assertFalse(in_array($userWithRole->email, $emailsList));
        $this->assertInstanceOf(LengthAwarePaginator::class, $userRepo->getNormalUsers(true));
    }

    /**
     * Test get drivers.
     */
    public function testGetDrivers()
    {
        $userRepo = app()->make(UserRepository::class);

        $nonDriverUser = factory(User::class)->create();
        $driverUser = factory(User::class)->create();
        try {
            $role = Role::whereName('driver')->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $role = factory(Role::class)->create(['name' => 'driver']);
        }
        $permission = \DB::table('permissions')->where('name', '=', 'driver')->first();

        $role->perms()->sync([$permission->id]);
        $driverUser->roles()->sync([$role->id]);

        $driversList = $userRepo->getDrivers();
        $emailsList = $driversList->pluck('email')->toArray();

        $this->assertInstanceOf(Collection::class, $driversList);
        $this->assertTrue(in_array($driverUser->email, $emailsList));
        $this->assertFalse(in_array($nonDriverUser->email, $emailsList));
    }

    public function testGetExpiredDrivers()
    {
        /** @var UserRepository $userRepo */
        $userRepo = app()->make(UserRepository::class);

        $driverUser = factory(User::class)->create();
        try {
            $role = Role::whereName('driver')->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $role = factory(Role::class)->create(['name' => 'driver']);
        }
        $store = factory(Store::class)->create();
        $permission = \DB::table('permissions')->where('name', '=', 'driver')->first();

        $role->perms()->sync([$permission->id]);
        $driverUser->roles()->sync([$role->id]);

        $driversList = $userRepo->getExpiredDrivers();
        $emailsList = $driversList->pluck('email')->toArray();
        // driver without any store
        $this->assertInstanceOf(Collection::class, $driversList);
        $this->assertFalse(in_array($driverUser->email, $emailsList));

        $driverUser->stores()->sync([$store->id => ['expire_at' => Carbon::now()->addHours(config('site.driverStoreExpireHours') + 1)]]);
        $driversList = $userRepo->getExpiredDrivers();
        $emailsList = $driversList->pluck('email')->toArray();
        // driver with store, but still active
        $this->assertInstanceOf(Collection::class, $driversList);
        $this->assertFalse(in_array($driverUser->email, $emailsList));

        $driverUser->stores()->sync([$store->id => ['expire_at' => Carbon::now()->subHours(999)]]);
        $driversList = $userRepo->getExpiredDrivers();
        $emailsList = $driversList->pluck('email')->toArray();
        // driver with store, and not active
        $this->assertInstanceOf(Collection::class, $driversList);
        $this->assertTrue(in_array($driverUser->email, $emailsList));
    }

    public function testGetAvailableDrivers()
    {
        /** @var UserRepository $userRepo */
        $userRepo = app()->make(UserRepository::class);

        $driverUser = factory(User::class)->create();
        try {
            $role = Role::whereName('driver')->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $role = factory(Role::class)->create(['name' => 'driver']);
        }
        $permission = \DB::table('permissions')->where('name', '=', 'driver')->first();
        $store = factory(Store::class)->create();

        $role->perms()->sync([$permission->id]);
        $driverUser->roles()->sync([$role->id]);

        //driver not available to be assigned to store
        $driversList = $userRepo->getAvailableDrivers($store->id);
        $emailsList = $driversList->pluck('email')->toArray();

        $this->assertInstanceOf(Collection::class, $driversList);
        $this->assertFalse(in_array($driverUser->email, $emailsList));

        //driver available to be assigned to store

        $driverUser->storesAssignments()->sync([$store->id]);

        $driversList = $userRepo->getAvailableDrivers($store->id);

        $emailsList = $driversList->pluck('email')->toArray();

        $this->assertInstanceOf(Collection::class, $driversList);
        $this->assertTrue(in_array($driverUser->email, $emailsList));
    }

    /** @test */
    function returns_many_users_by_ids()
    {
        $userA = factory(User::class)->create();
        $userB = factory(User::class)->create();
        $userC = factory(User::class)->create();

        $userRepo = app()->make(UserRepository::class);

        $users = $userRepo->getUsersByIds([$userA->id, $userC->id]);

        $this->assertCount(1, $users->where('id', $userA->id));
        $this->assertCount(0, $users->where('id', $userB->id));
        $this->assertCount(1, $users->where('id', $userC->id));
    }

    /** @test */
    function returns_drivers_delivering_today_orders_of_single_store()
    {
        $storeX = factory(Store::class)->create();
        $storeY = factory(Store::class)->create();

        $driverXA = DriverFactoryHelper::createWithRole();
        $driverXB = DriverFactoryHelper::createWithRole();
        $driverXC = DriverFactoryHelper::createWithRole();
        $driverYA = DriverFactoryHelper::createWithRole();
        $driverYB = DriverFactoryHelper::createWithRole();

        $orderXA = factory(Order::class)->create(['driver_id' => $driverXA, 'store_id' => $storeX->id, 'expected_at' => Carbon::today()]);
        $orderXB = factory(Order::class)->create(['driver_id' => $driverXB, 'store_id' => $storeX->id, 'expected_at' => Carbon::yesterday()]);
        $orderXC = factory(Order::class)->create(['driver_id' => $driverXC, 'store_id' => $storeX->id, 'expected_at' => Carbon::today()]);
        $orderYA = factory(Order::class)->create(['driver_id' => $driverYA, 'store_id' => $storeY->id, 'expected_at' => Carbon::today()]);
        $orderYB = factory(Order::class)->create(['driver_id' => $driverYB, 'store_id' => $storeY->id, 'expected_at' => Carbon::yesterday()]);

        $userRepo = app()->make(UserRepository::class);
        $drivers = $userRepo->getDriversOfTodayOrders($storeX->id);

        $this->assertTrue($drivers->contains('id', $driverXA->id));
        $this->assertFalse($drivers->contains('id', $driverXB->id));
        $this->assertTrue($drivers->contains('id', $driverXC->id));
        $this->assertFalse($drivers->contains('id', $driverYA->id));
        $this->assertFalse($drivers->contains('id', $driverYB->id));
    }
}
