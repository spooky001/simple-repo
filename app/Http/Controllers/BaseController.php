<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class BaseController.
 *
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{


    /**
     * BaseController constructor.
     */
    public function __construct()
    {

    }

}
