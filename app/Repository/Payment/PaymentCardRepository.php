<?php

namespace App\Repository\Payment;

use Illuminate\Database\Eloquent\Collection;
use App\Models\PaymentCard;
use App\Repository\BaseRepository;
/**
 * Class PaymentCardRepository.
 *
 * @package App\Repository
 */
class PaymentCardRepositorysitory extends BaseRepository
{
    /**
     * Initialize repository instance.
     *
     * @param PaymentCard $model
     */
    public function __construct(PaymentCard $model)
    {
        $this->model = $model;
    }

    /**
       * Add a new card
       *
       * @return PaymentCard
       */
    public function createPaymentCard(array $data): PaymentCard
    {
        $rules = [
            'number' => ['required']
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return $this->model->create($data);
    }

    /**
     * Get cc list.
     *
     * @return PaymentCard[]|\Illuminate\Support\Collection
     */
    public function findByUser(int $userId): Collection
    {
        return $this->model->where('user_id', '=', $userId)->get();
    }

     /**
     * Get a card.
     *
     * @return PaymentCard[]|\Illuminate\Support\Collection
     */
    public function findByUserAndNumber(int $userId, String $number)
    {
        return $this->model->where('user_id', '=', $userId)->where('number', '=', $number)->first();
    }

    /**
     * Find card by number
     *
     * @return PaymentCard
     */
    public function findByNumber(String $number): Collection
    {
        return $this->model->where('number', '=', $number)->get();
    }
}
