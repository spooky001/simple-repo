<?php

namespace Tests\Models;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use App\Models\Store;
use DriverFactoryHelper;
use App\Models\Permission;
use App\Models\UserAddress;
use App\Models\PaymentCard;
use App\Events\DriverLoggedOut;
use App\Models\OAuthAccessToken;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection as SupportCollection;
use App\Exceptions\DriverDoesNotHaveActiveStoreException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class UserTest.
 */
class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test user addresses.
     */
    public function testUserPaymentCards()
    {
        $userModel = app()->make(User::class);

        $this->assertInstanceOf(Collection::class, $userModel->paymentCards);
        $this->assertInstanceOf(HasMany::class, $userModel->paymentCards());
        $this->assertEquals(0, factory(User::class)->create()->paymentCards->count());

        $user = factory(User::class)->create();
        factory(PaymentCard::class)->create(['user_id' => $user->id]);

        $this->assertEquals(1, $user->paymentCards->count());
    }
}
