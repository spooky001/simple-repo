<?php

namespace Tests\Models;


use Tests\TestCase;
use App\Models\PaymentCard;
use App\Models\User as User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class PaymentCardTest.
 */
class PaymentCardTest extends TestCase
{
    use RefreshDatabase;

     /**
     * Test user.
     */
    public function testUser()
    {
        $paymentCardModel = app()->make(PaymentCard::class);
        $userId =  factory(User::class)->create()->id;

        $this->assertNull($paymentCardModel->user);
        $this->assertInstanceOf(
            User::class,
            factory(PaymentCard::class)->create(['user_id' => $userId])->user
        );

        $this->assertInstanceOf(BelongsTo::class, $paymentCardModel->user());
    }

    public function testPaymentCard()
    {
        $model = factory(PaymentCard::class)->create();
        $this->assertInstanceOf(PaymentCard::class, $model);
    }

}
