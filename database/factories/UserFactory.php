<?php

use App\Models\User;
use Faker\Generator;

$factory->define(User::class, function (Generator $faker) {
    return [
        'login' => strtolower($faker->firstNameMale),
        'email' => $faker->email,
        'password' => \Hash::make($faker->word),
        'phone' => $faker->randomNumber(9),
        'firstname' => $faker->firstNameMale,
        'lastname' => $faker->lastName,
        'info' => $faker->text,
    ];
});
