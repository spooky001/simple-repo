<?php

namespace Tests;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\UploadedFile;
use PHPUnit\Framework\Assert as PHPUnit;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase.
 */
class TestCase extends BaseTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * @var User
     */
    protected $loginUser;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Set up.
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('migrate');
    }

    /**
     * Tear down.
     */
    public function tearDown()
    {
        parent::tearDown();

        $refl = new \ReflectionObject($this);

        foreach ($refl->getProperties() as $prop) {
            if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
                $prop->setAccessible(true);
                $prop->setValue($this, null);
            }
        }
    }

    /**
     * Generate uploaded file.
     *
     * @return UploadedFile
     */
    protected function generateUploadedFile()
    {
        return UploadedFile::fake()->image('faker.jpg');
    }

    /**
     * Get method.
     *
     * @param $class
     * @param $method
     *
     * @return \ReflectionMethod
     */
    protected function getMethod($class, $method)
    {
        $method = (new \ReflectionClass(get_class($class)))->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * Get variable.
     *
     * @param $class
     * @param $variable
     *
     * @return \ReflectionProperty
     */
    protected function getVariable($class, $variable)
    {
        $variableReflection = new \ReflectionProperty(get_class($class), $variable);
        $variableReflection->setAccessible(true);

        return $variableReflection;
    }

    /**
     * Login.
     *
     * @param bool $user
     */
    protected function login($user = false)
    {
        if (!$user) {
            $user = factory(User::class)->create();
        }

        $this->loginUser = $user;

        $this->be($user);
    }

    /**
     * @return void
     */
    protected function pass(): void
    {
        $this->assertTrue(true);
    }

    /**
     * @param $response
     * @param $keys
     * @return void
     */
    public function assertJsonValidationErrors($response, $keys): void
    {
        $errors = $response->json()['errors'];

        foreach (Arr::wrap($keys) as $key) {
            PHPUnit::assertTrue(
                isset($errors[$key]),
                "Failed to find a validation error in the response for key: '{$key}'"
            );
        }
    }
}
