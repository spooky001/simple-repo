<?php

namespace App\Repository;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepo.
 *
 * @package App\Repository
 */
abstract class BaseRepository
{
    /**
     * @var mixed
     */
    protected $model;

    /**
     * Get all.
     *
     * @return Model[]|Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get by id.
     *
     * @param int $id
     * @param bool $orFail
     *
     * @return Model
     */
    public function getById($id, $orFail = true)
    {
        if ($orFail) {
            return $this->model->findOrFail($id);
        }

        return $this->model->find($id);
    }

    /**
     * Create.
     *
     * @param array $attributes
     *
     * @return Model
     */
    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Update.
     *
     * @param int $id
     * @param array $data
     *
     * @return bool|int
     */
    public function update($id, array $data)
    {
        /** @var Model $object */
        $object = $this->model->find($id);

        if (is_null($object)) {
            return false;
        }

        return $object->update($data);
    }

    /**
     * Delete.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        return $this->getById($id)->delete();
    }
}
