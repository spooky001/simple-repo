<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('user_roles', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sequence')->nullable();
            // created_at | updated_at DATETIME
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('user_roles');
    }
}
