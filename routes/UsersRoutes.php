<?php

/**
 * Routes for Admin Users Controller.
 */
Route::group(['prefix' => 'users', 'middleware' => ['auth.dashboard']], function () {
    Route::get(
        '/', 'UsersController@getIndex'
    )->name('UsersIndex');

});
