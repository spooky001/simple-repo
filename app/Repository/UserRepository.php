<?php

namespace App\Repository;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Store;
use App\Models\Permission;
use Illuminate\Support\Collection;

/**
 * Class UserRepository.
 */
class UserRepositorysitory extends BaseRepository
{
    /**
     * Initialize repository instance.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Search a customer by their number.
     *
     * @param string|int $query
     * @param int $limit
     *
     * @return \Illuminate\Database\Eloquent\Collection|User[]
     */
    public function findUser($query, $limit = 5)
    {
        /* @var User[] $users */
        return User::where('phone', '=', $query)
            ->where('active', '=', 1)
            ->limit($limit)
            ->get();
    }

    /**
     * Update user info.
     *
     * @param int $userId
     * @param string $internalNotes
     *
     * @return User
     */
    public function updateInfo($userId, $internalNotes)
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
        $user->internal_notes = $internalNotes;
        $user->save();

        return $user;
    }

     /**
     * Update user names
     *
     * @param int $userId
     * @param string $fistname;
     *
     * @return User
     */
    public function updateNames(int $userId, string $firstname, string $lastname): User
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->save();

        return $user;
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function getUsersByIds(array $ids): Collection
    {
        return User::whereIn('id', $ids)->get();
    }
}
