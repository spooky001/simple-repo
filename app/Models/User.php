<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Collection;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User.
 *
 * @package App\Models
 *
 * @property Carbon $seen_at
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'email',
        'password',
        'firstname',
        'lastname',
        'phone',
        'info',
        'active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'int',
        'active' => 'bool',
    ];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Payment Card relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|PaymentCard[]
     */
    public function paymentsCards()
    {
        return $this->hasMany(PaymentCard::class);
    }
}
