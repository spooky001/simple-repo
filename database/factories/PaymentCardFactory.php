<?php

use Carbon\Carbon;
use App\Models\User;
use App\Models\PaymentCard;
use Faker\Generator;

$factory->define(PaymentCard::class, function (Generator $faker) {
    return [
        'number' => mt_rand(1000, 9999)."****",
        'token' => mt_rand(1000, 9999),
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});
